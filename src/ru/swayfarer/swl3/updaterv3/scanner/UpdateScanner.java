package ru.swayfarer.swl3.updaterv3.scanner;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;

@Data
@Accessors(chain = true)
public class UpdateScanner {
    
    @NonNull
    public UpdaterContext updateContext;
    
    public UpdatableContent scan(@NonNull UpdateScanContext updateScanContext)
    {
        var content = new UpdatableContent();
        var scanRootDir = updateScanContext.getRootDir();
        var filtering = updateScanContext.getContentInfo().getFiltering();
        
        var scanEvents = updateContext.getScanEvents();
        
        var filesToScan = scanRootDir.findAllSubfiles()
                .filter((f) -> filtering.isMatches(updateScanContext.getFileLocalPath(f)))
        ;
        
        var scanStartEvent = ScanStartEvent.builder()
                .filesToScan(filesToScan)
                .scanContext(updateScanContext)
                .build()
        ;
        
        scanEvents.eventScanStart.next(scanStartEvent);
        
        for (var fileToScan : filesToScan)
        {
            var entry = scanFile(updateScanContext, fileToScan);
            
            if (entry != null)
            {
                content.getFiles().add(entry);
            }
        }
        
        var scanEndEvent = ScanCompleteEvent.builder()
                .resultContent(content)
                .scanContext(updateScanContext)
                .build()
        ;
        
        scanEvents.eventScanEnd.next(scanEndEvent);
        
        content.setUpdatableContentInfo(updateScanContext.getContentInfo());
        
        return content;
    }
    
    public String getHash(String hashingType, @NonNull FileSWL file)
    {
        if (file.isDirectory())
        {
            return "<dir>";
        }
      
        return updateContext.getCryptoUtils().hash()
            .algorithm(StringUtils.orDefault(hashingType, "MD5"))
            .in(file.in())
            .asHEX()
        ;
    }
    
    public FileEntry scanFile(@NonNull UpdateScanContext context, @NonNull FileSWL file)
    {
        var scanEvents = updateContext.getScanEvents();
        var contentInfo = context.getContentInfo();
        var filtering = contentInfo.getFiltering();
        var fileLocalPath = context.getFileLocalPath(file);

        if (filtering.isMatches(fileLocalPath))
        {
            var fileHash = getHash(contentInfo.getHashingType(), file);
            
            if (file.isDirectory())
            {
                if (!fileLocalPath.endsWith("/"))
                {
                    fileLocalPath += "/";
                }
            }
            
            var fileEntry = FileEntry.builder()
                    .hash(fileHash)
                    .path(fileLocalPath)
                    .isDirectory(file.isDirectory() ? true : null)
                    .sizeInBytes(file.isDirectory() ? null : file.length())
                    .source("f:" + file.getAbsolutePath())
                    .build()
            ;
            
            var entryScannedEvent = ScanFileEvent.builder()
                    .scannedEntry(fileEntry)
                    .scannedFile(file)
                    .scanContext(context)
                    .build()
            ;
            
            System.out.println("Scanned " + fileLocalPath);
            
            scanEvents.eventFileEntryScanned.next(entryScannedEvent);
            
            return fileEntry;
        }
        
        return null;
    }
}
