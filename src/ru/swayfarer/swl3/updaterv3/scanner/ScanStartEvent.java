package ru.swayfarer.swl3.updaterv3.scanner;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors
@SuperBuilder
public class ScanStartEvent extends AbstractEvent{
    
    @Builder.Default
    @NonNull
    public ExtendedList<FileSWL> filesToScan = new ExtendedList<>();
    
    @NonNull
    public UpdateScanContext scanContext;
}
