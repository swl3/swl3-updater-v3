package ru.swayfarer.swl3.updaterv3.scanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContentInfo;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class UpdateScanContext {
    
    @NonNull
    public UpdatableContentInfo contentInfo;
    
    @NonNull
    public FileSWL rootDir;
    
    public String getFileLocalPath(@NonNull FileSWL file)
    {
        return file.getLocalPath(rootDir);
    }
}
