package ru.swayfarer.swl3.updaterv3.scanner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ScanCompleteEvent {
    
    @NonNull
    public UpdateScanContext scanContext;
    
    @NonNull
    public UpdatableContent resultContent;
}
