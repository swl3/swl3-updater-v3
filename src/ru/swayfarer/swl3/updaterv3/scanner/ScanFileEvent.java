package ru.swayfarer.swl3.updaterv3.scanner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors
@SuperBuilder
public class ScanFileEvent extends AbstractEvent {
    
    @NonNull
    public UpdateScanContext scanContext;
    
    @NonNull
    public FileEntry scannedEntry;
    
    @NonNull
    public FileSWL scannedFile;
}
