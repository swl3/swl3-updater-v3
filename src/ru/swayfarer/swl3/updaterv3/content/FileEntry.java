package ru.swayfarer.swl3.updaterv3.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FileEntry {
    
    @NonNull
    public String hash;
    
    @NonNull
    public String path;
    
    public Long sizeInBytes;
    
    public Boolean isDirectory;
    
    public String source;
    
    public boolean isDirectory()
    {
        return Boolean.TRUE.equals(isDirectory);
    }
    
    public long getSizeInBytes()
    {
        return sizeInBytes == null ? -1 : sizeInBytes;
    }
    
    public static FileEntry dummy(@NonNull String path, Boolean isDirectory)
    {
        return builder()
                .hash("<dummy_entry>")
                .path(path)
                .source("<dummy_entry>")
                .isDirectory(isDirectory)
                .build()
        ;
    }
}
