package ru.swayfarer.swl3.updaterv3.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UpdatableContent {
    
    @NonNull
    public UpdatableContentInfo updatableContentInfo;
    
    @NonNull
    public ExtendedList<FileEntry> files = new ExtendedList<>().synchronyzed(); 
    
    public UpdatableContent filter()
    {
        files.removeIf((entry) -> !updatableContentInfo.filtering.isMatches(entry.path));
        return this;
    }
}
