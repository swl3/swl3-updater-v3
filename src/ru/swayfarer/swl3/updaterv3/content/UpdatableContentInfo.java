package ru.swayfarer.swl3.updaterv3.content;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.string.FilteringList;

@Data
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UpdatableContentInfo {
    
    public String name;
    
    public String hashingType;
    
    @Builder.Default
    public FilteringList filtering = new FilteringList();
}
