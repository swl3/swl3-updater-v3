
package ru.swayfarer.swl3.updaterv3.io;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

public interface IFileEntryUploader extends IFunction3<FileEntry, IFunction0<DataInStream>, RefreshEvent, String> {
    
    @Override
    default String applyUnsafe(FileEntry fileEntry, IFunction0<DataInStream> streamSource, @NonNull RefreshEvent refreshEvent) throws Throwable
    {
        return upload(fileEntry, streamSource, refreshEvent);
    }
    
    public String upload(FileEntry fileEntry, IFunction0<DataInStream> streamSource, RefreshEvent refreshEvent) throws Throwable;
}
