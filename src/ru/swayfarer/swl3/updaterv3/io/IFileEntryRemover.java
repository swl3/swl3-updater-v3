package ru.swayfarer.swl3.updaterv3.io;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

public interface IFileEntryRemover extends IFunction2NoR<FileEntry, RefreshEvent>{
    
    @Override
    default void applyNoRUnsafe(FileEntry fileEntry, @NonNull RefreshEvent refreshEvent) throws Throwable
    {
        remove(fileEntry, refreshEvent);
    }
    
    void remove(FileEntry fileEntry, @NonNull RefreshEvent refreshEvent) throws Throwable;
}
