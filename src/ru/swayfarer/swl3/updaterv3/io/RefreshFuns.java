package ru.swayfarer.swl3.updaterv3.io;

import lombok.NonNull;

public class RefreshFuns {

    public static IFileEntryRemover retry(long maxRetryCount, long delay, @NonNull IFileEntryRemover fun)
    {
        return (fileEntry, event) -> {
            var lastRetry = maxRetryCount - 1;
            
            for (int i1 = 0; i1 < maxRetryCount; i1 ++)
            {
                try
                {
                    fun.apply(fileEntry, event);
                }
                catch (Throwable e)
                {
                    if (i1 < lastRetry)
                        Thread.sleep(delay);
                    else
                        throw e;
                }
            }
        };
    }
    
    public static IFileEntryUploader retry(long maxRetryCount, long delay, @NonNull IFileEntryUploader fun)
    {
        return (fileEntry, streamFun, event) -> {
            var lastRetry = maxRetryCount - 1;
            
            for (int i1 = 0; i1 < maxRetryCount; i1 ++)
            {
                try
                {
                    var ret = fun.apply(fileEntry, streamFun, event);
                    return ret;
                }
                catch (Throwable e)
                {
                    if (i1 < lastRetry)
                        Thread.sleep(delay);
                    else
                        throw e;
                }
            }
            
            return "<error>";
        };
    }
}
