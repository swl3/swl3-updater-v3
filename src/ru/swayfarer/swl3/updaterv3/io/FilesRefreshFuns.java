package ru.swayfarer.swl3.updaterv3.io;

import lombok.NonNull;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

public class FilesRefreshFuns {
    
    public static IFileEntryUploader upload()
    {
        return upload((i) -> {});
    }
    
    public static IFileEntryUploader upload(IFunction1NoR<Long> progressTracker)
    {
        return (fileEntry, streamFun, refreshEvent) -> {
            try
            {
                var rootDir = getRootDir(refreshEvent);
                ExceptionsUtils.IfNull(rootDir, IllegalStateException.class, "Root dir was not setted!");
                var file = rootDir.subFile(fileEntry.getPath());
                
                if (fileEntry.isDirectory())
                {
                    if (file.exists() && !file.isDirectory())
                        file.remove();
                    
                    file.createIfNotFoundDir();
                }
                else
                {
                    file.createIfNotFound();
                    var inStream = streamFun.apply();
                    
                    if (inStream == null)
                    {
                        var exception = new RefreshFileEntryException("Can't open input stream for update " + fileEntry);
                        exception.setFileEntry(fileEntry);
                        
                        throw exception;
                    }
                    
                    StreamsUtils.copyStream(inStream, file.out(), progressTracker, true, true);
                }
                
                return "f:" + file.getAbsolutePath();
            }
            catch (Throwable e)
            {
                if (e instanceof RefreshFileEntryException)
                    throw e;
                
                var ex = new RefreshFileEntryException(e);
                ex.setFileEntry(fileEntry);
                throw ex;
            }
        };
    }
    
    public static IFileEntryRemover remove()
    {
        return (fileEntry, refreshEvent) -> {
            var rootDir = getRootDir(refreshEvent);
            
            ExceptionsUtils.IfNull(rootDir, IllegalStateException.class, "Root dir was not setted!");
            
            var file = rootDir.subFile(fileEntry.getPath());
            
            if (file != null)
            {
                file.remove();
            }
        };
    }
    public static FileSWL getRootDir(@NonNull AbstractEvent event)
    {
        return event.getCustomAttributes().getValue("localRootDir");
    }
    
    public static void setRootDir(@NonNull AbstractEvent event, @NonNull FileSWL file)
    {
        event.getCustomAttributes().put("localRootDir", file);
    }
}
