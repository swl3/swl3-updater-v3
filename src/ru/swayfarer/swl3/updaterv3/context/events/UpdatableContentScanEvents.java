package ru.swayfarer.swl3.updaterv3.context.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.updaterv3.scanner.ScanCompleteEvent;
import ru.swayfarer.swl3.updaterv3.scanner.ScanFileEvent;
import ru.swayfarer.swl3.updaterv3.scanner.ScanStartEvent;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class UpdatableContentScanEvents {
    
    @NonNull
    @Builder.Default
    public IObservable<ScanStartEvent> eventScanStart = Observables.createObservable();
    
    @NonNull
    @Builder.Default
    public IObservable<ScanFileEvent> eventFileEntryScanned = Observables.createObservable();
    
    @NonNull
    @Builder.Default
    public IObservable<ScanCompleteEvent> eventScanEnd = Observables.createObservable();
}
