package ru.swayfarer.swl3.updaterv3.context.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class UpdatableContentEvents {
   
    @NonNull
    @Builder.Default
    public IObservable<FileEntry> eventFileComplete = Observables.createObservable();
    
    @NonNull
    @Builder.Default
    public IObservable<RefreshFileEntryException> eventFileFail = Observables.createObservable();
    
    @NonNull
    @Builder.Default
    public IObservable<UpdatableContent> eventContentStart = Observables.createObservable();
    
    @NonNull
    @Builder.Default
    public IObservable<UpdatableContent> eventContentEnd = Observables.createObservable();
}
