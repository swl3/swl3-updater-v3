package ru.swayfarer.swl3.updaterv3.context;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.crypto.CryptoUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.events.UpdatableContentEvents;
import ru.swayfarer.swl3.updaterv3.context.events.UpdatableContentScanEvents;
import ru.swayfarer.swl3.updaterv3.io.IFileEntryRemover;
import ru.swayfarer.swl3.updaterv3.io.IFileEntryUploader;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class UpdaterContext {
    
    @NonNull
    public RLUtils rlUtils;
    
    @NonNull
    public CryptoUtils cryptoUtils;
    
    @NonNull
    public ExceptionsHandler exceptionsHandler;
    
    @NonNull
    public IFileEntryUploader fileEntryUploader;
    
    @NonNull
    public IFileEntryRemover fileEntryRemover;
    
    @Builder.Default
    @NonNull
    public UpdatableContentEvents events = new UpdatableContentEvents();
    
    @Builder.Default
    @NonNull
    public UpdatableContentScanEvents scanEvents = new UpdatableContentScanEvents();
    
    public DataInStream getInputStream(@NonNull FileEntry fileEntry)
    {
        return rlUtils.createLink(fileEntry.getSource()).in();
    }
}
