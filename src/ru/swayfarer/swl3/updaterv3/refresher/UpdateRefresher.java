package ru.swayfarer.swl3.updaterv3.refresher;

import java.util.stream.Collectors;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.threads.lock.CounterLock;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

@Data
@Accessors(chain = true)
public class UpdateRefresher {
    
    @NonNull
    public UpdaterContext updateContext;
    
    @NonNull
    public CounterLock endRefreshLock = new CounterLock();
    
    @NonNull
    public IFunction1NoR<IFunction0NoR> actionExecutor = (e) -> e.apply();
    
    public void refreshTargetAll(@NonNull RefreshEvent refreshEvent)
    {
        updateContext.getEvents().eventContentStart.next(refreshEvent.getModelContent());
        
        removeNonExistings(refreshEvent);
        removeMirroredDirsAndFiles(refreshEvent);
        refreshExistingAndDownload(refreshEvent);
        
        endRefreshLock.waitFor();
        
        execute(() -> {
            updateContext.getEvents().eventContentEnd.next(refreshEvent.getModelContent());
        });
    }
    
    public void refreshExistingAndDownload(@NonNull RefreshEvent event)
    {
        var exceptionsHandler = updateContext.getExceptionsHandler();
        
        var localContent = event.getTargetContent();
        var remoteContent = event.getModelContent();
        
        var localFiles = localContent.getFiles().exStream()
                .toMap((entry) -> entry.getPath(), (entry) -> entry)
        ;

        for (var remoteFileEntry : remoteContent.getFiles())
        {
            try
            {
                var localFileEntryFound = true;
                var localFileEntry = localFiles.get(remoteFileEntry.getPath());
                
                if (localFileEntry == null)
                {
                    localFileEntryFound = false;
                    localFileEntry = FileEntry.dummy(
                            remoteFileEntry.getPath(), 
                            remoteFileEntry.isDirectory()
                    );
                    localFileEntry.setSizeInBytes(remoteFileEntry.getSizeInBytes());
                    localContent.getFiles().add(localFileEntry);
                }
                
                if (!isLocalActual(remoteFileEntry, localFileEntry))
                {
                    var finalLocalEntry = localFileEntry;
                    
                    if (localFileEntryFound)
                    {
                        execute(() -> {
                            updateContext.getFileEntryRemover().remove(finalLocalEntry, event);
                        });
                    }
                    
                    var rlutils = updateContext.getRlUtils();
                    
                    execute(() -> {
                        var is = !remoteFileEntry.isDirectory() ? rlutils.createLink(remoteFileEntry.getSource()).in() : null;
                        var newLocationRlink = updateContext.getFileEntryUploader().apply(finalLocalEntry, () -> is, event);
                        updateContext.getEvents().eventFileComplete.next(remoteFileEntry);
                        
                        finalLocalEntry.setSource(newLocationRlink);
                        finalLocalEntry.setHash(remoteFileEntry.getHash());
                    });
                }
                else
                {
                    updateContext.getEvents().eventFileComplete.next(remoteFileEntry);
                }
                
            }
            catch (Throwable e)
            {
                
                var entryException = new RefreshFileEntryException(e);
                entryException.setFileEntry(remoteFileEntry);
                
                updateContext.getEvents().eventFileFail.next(entryException);
                exceptionsHandler.handle(
                        entryException, 
                        "Error while updating file", remoteFileEntry.getPath(), "with remote hash", remoteFileEntry.getHash()
                );
            }
        }
    }
    
    public boolean isLocalActual(@NonNull FileEntry remoteFile, @NonNull FileEntry localFile)
    {
        if (!localFile.getHash().equals(remoteFile.getHash()))
            return false;
        
        if (localFile.isDirectory() != remoteFile.isDirectory())
            return false;

        return true;
    }
    
    public void removeMirroredDirsAndFiles(@NonNull RefreshEvent event)
    {
        var modelContent = event.getModelContent();
        var targetContent = event.getTargetContent();
        var removeFun = updateContext.getFileEntryRemover();
        
        var modelFiles = modelContent.getFiles()
                .toMap(FileEntry::getPath)
        ;
        
        var iterator = targetContent.getFiles().iterator();
        
        while (iterator.hasNext())
        {
            var targetFileEntry = iterator.next();
            
            if (targetFileEntry.isDirectory())
            {
                var modelFileEntry = modelFiles.getValue(targetFileEntry.getPath());
                
                if (modelFileEntry == null || !modelFileEntry.isDirectory())
                {
                    execute(() -> {
                        removeFun.apply(targetFileEntry, event);
                    });
                    
                    iterator.remove();
                }
            }
        }
    }
    
    public void removeNonExistings(@NonNull RefreshEvent event)
    {
        var modelContent = event.getModelContent();
        var targetContent = event.getTargetContent();
        var removeFun = updateContext.getFileEntryRemover();
        
        var modelFiles = modelContent.getFiles().exStream()
                .map((entry) -> entry.getPath())
                .collect(Collectors.toSet())
        ;
        
        var iterator = targetContent.getFiles().iterator();
        
        while (iterator.hasNext())
        {
            var targetFileEntry = iterator.next();
            
            if (!modelFiles.contains(targetFileEntry.getPath()))
            {
                iterator.remove();
                execute(() -> {
                    removeFun.apply(targetFileEntry, event);
                });
            }
        }
    }
    
    public void execute(@NonNull IFunction0NoR fun)
    {
        endRefreshLock.increment();
        
        actionExecutor.apply(() -> {
            try
            {
                fun.apply();
            }
            finally
            {
                endRefreshLock.reduce();
            }
        });
    }
}
