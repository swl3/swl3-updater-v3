package ru.swayfarer.swl3.updaterv3.refresher;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;

@Data 
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RefreshEvent extends AbstractEvent{
    
    @NonNull 
    public UpdatableContent modelContent;
    
    @NonNull
    public UpdatableContent targetContent;
}
