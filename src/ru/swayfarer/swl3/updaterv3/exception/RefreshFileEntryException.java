package ru.swayfarer.swl3.updaterv3.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;

@Getter @Setter
@Accessors(chain = true)
@SuppressWarnings("serial")
public class RefreshFileEntryException extends RuntimeException {

    public FileEntry fileEntry;
    
    public RefreshFileEntryException()
    {
        super();
    }

    public RefreshFileEntryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RefreshFileEntryException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public RefreshFileEntryException(String message)
    {
        super(message);
    }

    public RefreshFileEntryException(Throwable cause)
    {
        super(cause);
    }
}
